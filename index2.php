
<?php
include ("koneksi.php");
include ("header.php");

?>

<html lang="en">
<head>
<title>Suratku</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> 
      <script src="js/jquery-3.2.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
<style>
/* side navigation styles */
body{
  margin :0;
  padding: 0;
  background-size: cover;
  font-family: sans-serif;
  background-image: url(bg.jpg);
}
.side-nav {
  position: absolute;
  width: 100%;
  height: 100vh;
  background-color: #294b4f;
  z-index: 1;
  display: none;
}
.side-nav.visible {
  display: block;
}
.side-nav ul {
  margin: 0;
  padding: 0;
}
.side-nav ul li {
  padding: 5px 5px;
  border-bottom:;
  position: relative;
}

.side-nav ul li a {
  color: #fff;
  display: block;
  text-decoration: none;
  font-family: Tahoma ;

}
.side-nav ul li i {
  color: #0497df;
  min-width: 20px;
  text-align: center;
    font-color : white;
}
.side-nav ul li span:nth-child(2) {
  margin-left: 10px;
  font-size: 14px;
  font-weight: 600;
}
/* main content styles */

/* set element styles to fit tablet and higher(desktop) */
@media screen and (min-width: 600px) {
  .header {
    background-color: #35475e;
    z-index: 1;
  }
  .header .logo {
    display: none;
  }
  .nav-trigger {
    display: none;
  }
  .nav-trigger span, .nav-trigger span:before, span:after {
    background-color: #fff;
  }
  .side-nav {
    display: block;
    width: 70px;
    z-index: 2;
  }
  .side-nav ul li span:nth-child(2) {
    display: none;
  }
  .side-nav .logo i {
    padding-left: 12px;
  }
  .side-nav .logo span {
    display: none;
  }
  .side-nav ul li i {
    font-size: 26px;
  }
  .side-nav ul li a {
    text-align: center;
  }
  .main-content {
    margin-left: 70px;
  }
}
/* set element styles for desktop */
@media screen and (min-width: 800px) {
  .side-nav {
    width: 200px;
  }
  .side-nav ul li span:nth-child(2) {
    display: inline-block;
  }
  .side-nav ul li i {
    font-size: 16px;
  }
  .side-nav ul li a {
    text-align: left;
  }
  .side-nav .logo i {
    padding-left: 0;
  }
  .side-nav .logo span {
    display: inline-block;
  }
  .main-content {
    margin-left: 200px;
  }
}


</style>

  <div class="side-nav">
        <ul>
          <li class="active">
          <ul style='margin-top:25px'>
              <li>
                <a style='padding-top:8px; color:#fff; border-radius:0px; text-align:left; padding-left:15px' class="btn btn-success" href="Home.php">
                    <i style='margin-top:-11px; color:#fff'></i>
                    <i class="glyphicon glyphicon-Home " title ="Dashboard" style='color:white'></i>&nbsp&nbsp&nbsp<span>Dashboard</span>
                </a>
            </li>    

            <li>
                <a style='padding-top:8px; color:#fff; border-radius:0px; text-align:left; padding-left:15px' class="btn btn-danger" href="surat-masuk.php">
                    <i style='margin-top:-11px; color:#fff'></i>
                    <i class="glyphicon glyphicon-send " title ="Surat Masuk"style='color:white'></i>&nbsp&nbsp&nbsp<span>Surat Masuk</span>
                </a>
            </li>  
            <li>
                <a style='padding-top:8px; color:#fff; border-radius:0px; text-align:left; padding-left:15px' class="btn btn-info" href="surat-keluar.php">
                    <i style='margin-top:-11px; color:#fff'></i>
                    <i class="glyphicon glyphicon-send " title ="Surat Keluar " style='color:white'></i>&nbsp&nbsp&nbsp<span>Surat Keluar</span>
                </a>
            </li>

            <li>
                <a style='padding-top:8px; color:#fff; border-radius:0px; background:#929e31; text-align:left; padding-left:15px' class="btn " href="report-surat-masuk.php">
                    <i style='margin-top:-11px; color:#fff'></i>
                    <i class="glyphicon glyphicon-file " title ="Report"style='color:white'></i>&nbsp&nbsp&nbsp<span>Report Surat Masuk</span>
                </a>
            </li>   

            <li>
                <a style='padding-top:8px; color:#fff; border-radius:0px; background:#cf14db; text-align:left; padding-left:15px' class="btn " href="report-surat-keluar.php">
                    <i style='margin-top:-11px; color:#fff'></i>
                    <i class="glyphicon glyphicon-file " title ="Report"style='color:white'></i>&nbsp&nbsp&nbsp<span>Report Surat Keluar</span>
                </a>
            </li>  
                      <li>
                <a style='padding-top:8px; color:#fff; border-radius:0px; background:#ff8800; text-align:left; padding-left:15px' class="btn " href="jasa.php">
                    <i style='margin-top:-11px; color:#fff'></i>
                    <i class="glyphicon glyphicon-phone-alt " title ="Report"style='color:white'></i>&nbsp&nbsp&nbsp<span>Pengantar</span>
                </a>
            </li>  
                    
        </ul>
    </div>
    <?php
include "koneksi.php";
    ?>
</head>
</html>