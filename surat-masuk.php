<?php
include "koneksi.php";
include "index2.php";

?>

<title>Suratku</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> 
      <script src="js/jquery-3.2.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <style>

.panel-heading{
	height: 50px;
	font-size: 20px;
	font-family: arial;
}
.main-content .table{
	font-size: 13px;
	font-family: arial;
}
.form-control{
  border-radius: 0;
}
.row {
  margin-top: 20px;
  margin-left: 20px;
  position: cover;
}
.form-control{
  border-radius: 0;
}

</style>

<div class="main-content">
<div class="panel panel-info">
  <div class="panel-heading">
    <h1 class="panel-title" style="color:black; margin-top:7px; font-family:tahoma; "> Data Surat Masuk</h1>
    
  </div>
  <div class="row">

      
      <button type="submit" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-plus"></i>&nbsp&nbspInput Surat Masuk</button>&nbsp&nbsp
       <a class='btn btn-success' href='excel-surat-masuk.php'><i class="glyphicon glyphicon-print"></i>&nbsp&nbspPrint to Excel</a> &nbsp&nbsp
      <a class='btn btn-info' href='surat-masuk-pdf.php'><i class="glyphicon glyphicon-print"></i>&nbsp&nbspPrint to PDF</a>
   
      </div>
<!--        <?php

  include "koneksi.php";

           $carikode = mysqli_query($koneksi, "select max(no_surat) from surat_masuk") or die (mysqli_error());
           $datakode = mysqli_fetch_array($carikode);
           if ($datakode){
            $nilaikode = substr($datakode[0], 1);
            $kode = (int) $nilaikode;
            $kode = $kode + 1;
            $hasilkode =str_pad($kode, 3, "0", STR_PAD_LEFT) ."/SR/" .date("d")."/".date("M")."/".date("Y");
           } else {
            $hasilkode = "/SR/001";
          }
  ?> -->
  <div class="panel-body">
<center></center>
                 <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead class='alert-success'>
                    <tr>
                        <th><center>No</center></th>
                        <th><center>Nomor Surat</center></th>
                        <th><center>Surat Dari</center></th>
                        <th><center>Perihal</center></th>
                        <th ><center>Tanggal Surat</center></th>
                        <th><center>Pengirim Surat</center></th>
                        <th><center>Lokasi Arsip</center></th>
                        <th><center>Status</center></th>
                        <th><center>Aksi</center></th>
                    </tr>
                    </thead>
                    <tbody>
  </div>
</div>
</div></div></div>

<?php
include "koneksi.php";

$halaman = 7;
  $page = isset($_GET["halaman"]) ? (int)$_GET["halaman"] : 1;
  $mulai = ($page>1) ? ($page * $halaman) - $halaman : 0;
$result = mysqli_query($koneksi,"SELECT*FROM surat_masuk ORDER BY surat_masuk.no_surat ASC");
  $no = 1;
$total = mysqli_num_rows($result);
  $pages = ceil($total/$halaman);            
  $query = mysqli_query($koneksi, "select * from surat_masuk LIMIT $mulai, $halaman");
  $no =$mulai+0;

while($row=mysqli_fetch_assoc($query)){
  $no++;
  echo"

  <tr>
  <td><center>".$no."</center></td>
  <td><a href ='#'><center>".$row['no_surat']."</center></td>
  <td><center>".$row['nama_surat']."</center></td>
  <td><center>".$row['perihal']."</center></td>
  <td><center>".$row['tanggal_surat']."</center></td>
  <td><center>".$row['pengirim_surat']."</center></td>
  <td><center>".$row['pengirim_surat']."</center></td>
    <td>";
                if($row['status'] == 'pending'){
                  echo '<span class="label label-info">Pending</span>';
                }
                else if ($row['status'] == 'approve' ){
                  echo '<span class="label label-success">Approve</span>';
                }
                else if ($row['status'] == 'dalampengiriman' ){
                  echo '<span class="label label-danger">Dalam pengiriman</span>';
                }
                  else if ($row['status'] == 'dikirim' ){
                  echo '<span class="label label-warning">Dikirim</span>';
                }
                  else if ($row['status'] == 'diterima' ){
                  echo '<span class="label label-success">Di terima</span>';
                }
                  else if ($row['status'] == 'tu' ){
                  echo '<span class="label label-primary">Admin</span>';
                }
              echo "
                </td>

<td>
  <center><a href=edit3.php?no_surat=".$row['no_surat']."><span class='glyphicon glyphicon-pencil' aria-hidden='true' title='Edit'></span></a>&nbsp&nbsp
  <a href=\"delete.php?id=$row[no_surat]\"onClick=\"return confirm('Yakin Hapus $row[no_surat]?')\"><span class='glyphicon glyphicon-trash' aria-hidden='true'title='Hapus Data' ></span></a>&nbsp&nbsp
  <a href=print-surat-masuk.php?no_surat=".$row['no_surat']."><span class='glyphicon glyphicon-print' aria-hidden='true' title='Print PDF'></span></a>
  </center>
  </td>
  </tr>";

}

echo"
<table>
";
?>
<?php
 include "koneksi.php";
$sqlCommand = "SELECT COUNT(*) FROM surat_masuk"; 
$query = mysqli_query($koneksi, $sqlCommand) or die (mysqli_error()); 
$row = mysqli_fetch_row($query);
echo "Jumlah Data  : " . $row[0];
mysqli_free_result($query); 
mysqli_close($koneksi);
?>


<br><br>
<div class="btn-group" role="group" aria-label="">
  <?php for ($i=1; $i<=$pages ; $i++){ ?>
 &nbsp<a href="?halaman=<?php echo $i; ?>" class="btn btn-default"><?php echo $i; ?></a>
 
  <?php } ?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel">Isi Data Surat Masuk</h5>
<form enctype="multipart/form-data"  class="form-horizontal" action="" method="post">

<div class="form-group">
          <label class="col-sm-3 control-label">Nomor Surat :</label>
          <div class="col-sm-6">
            <input type="text" name="no_surat" value="" class="form-control" requied="requied">
          </div>
        </div>
<br>
<br>
<br>

        <div class="form-group">
          <label class="col-sm-3 control-label">Surat Dari  :</label>
          <div class="col-sm-6">
            <input type="text" name="nama_surat"  class="form-control" required>
          </div>
        </div>
<br>
<br>
<br>

        <div class="form-group">
          <label class="col-sm-3 control-label">Perihal :</label>
          <div class="col-sm-6">
            <input type="text" name="perihal"  class="form-control" required >
          </div>
        </div>
<br>
<br>
<br>


        <div class="form-group">
          <label class="col-sm-3 control-label">Tanggal Surat:</label>
          <div class="col-sm-6">
            <input type="date" name="tanggal_surat"  class="form-control" required >
          </div>
        </div>
<br>
<br>
<br>

        <div class="form-group">
          <label class="col-sm-3 control-label">Pengirim Surat:</label>
          <div class="col-sm-6">
            <input type="text" name="pengirim_surat"  class="form-control" required >
          </div>
        </div>
<br>
<br>
<br>

        <div class="form-group">
          <label class="col-sm-3 control-label">Keterangan  :</label>
          <div class="col-sm-6">
            <textarea type="text" name="keterangan"  class="form-control" required> </textarea>
          </div>
        </div>

<br>
<br>
<br>


                <div class="form-group">
          <label class="col-sm-3 control-label">Lokasi Arsip :</label>
          <div class="col-sm-6">
            <input type="text" name="lokasi"  class="form-control" required>
          </div>
        </div>  
        <br>
        <br>
       

              <div class="form-group">
              <label class="col-sm-3 control-label">Status :</label>
              <div class="col-sm-6">
              <select name="status" class="form-control">

                  <option value="pending">Pending</option>
                  <option value="approve">Approve</option>
                  <option value="dalampengiriman">Dalam Pengiriman</option>
                  <option value="dikirim">Di kirim</option>
                  <option value="diterima">Di terima</option>
                  <option value="tu">Admin</option>
                  </select>

              </div>
            </div>
              </div>

</div>


      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-primary" name="kirim" value="Simpan">
      </div>
      </form>
    </div>
</div>
</div>  
 <?php
           include "koneksi.php";
        //tambahan
        //end tambahan
        //error_reporting(0);
              if(isset($_POST['kirim'])){

                
        //$tggl_keluhan = $_POST['tggl_keluhan'];
                $no_surat = $_POST['no_surat'];
                $nama_surat = $_POST['nama_surat'];
                $perihal = $_POST['perihal'];
                $tanggal_surat = $_POST['tanggal_surat'];
                $pengirim_surat = $_POST['pengirim_surat'];
                $keterangan = $_POST['keterangan'];
                $lokasi = $_POST['lokasi'];

                $status = $_POST['status'];                
        
                $q = "INSERT INTO surat_masuk(no_surat,nama_surat,perihal,tanggal_surat,pengirim_surat,keterangan,lokasi,status)VALUES('$no_surat','$nama_surat','$perihal','$tanggal_surat','$pengirim_surat','$keterangan','$lokasi','$status')";

                $h = mysqli_query($koneksi,$q);

                ?>
                <script type="text/javascript">
                alert('data anda tersimpan');
                document.location.href = "surat-masuk.php";
                  </script>
                <?php

              }


              ?>
</html>