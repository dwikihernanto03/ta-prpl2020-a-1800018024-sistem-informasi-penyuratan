<?php
include ("koneksi.php");
include ("index2.php");

?>


<title>Suratku</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> 
      <script src="js/jquery-3.2.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <style>
.container .panel{
	height: 550px;
	overflow: scroll;
	width: 97%;
}
.container .panel-heading{
	height: 50px;
	font-size: 20px;
	font-family: arial;
}
.main-content .table{
	font-size: 13px;
	font-family: arial;
}
.form-control{
  border-radius: 0;
}
</style>

<div class="main-content">
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title"></h3>
  </div>
  <div class="panel-body">
<center><h1>Selamat Datang Di Aplikasi Surat</h1></center>
<style>
.bs-glyphicons {
  margin: 5px 20px 20px 0px;
  overflow: hidden;
}
.bs-glyphicons-list {
  padding-left: 0;
  list-style: none;
}

.bs-glyphicons-list a {
  color:#8a8a8a !important;
}
ol, ul {
  margin-top: 0;
  margin-bottom: 10px;
}
.bs-glyphicons li {
  float: left;
  width: 20%;
  height: 155px;
  padding: 30px;
  font-size: 10px;
  line-height: 1.4;
  text-align: center;
  border: 1px solid #cecece;
  background-color: #ffffff;
}
.bs-glyphicons li:hover {
  background-color: #e3e3e3;
}
.bs-glyphicons .glyphicon {
  margin-top: 0px;
  margin-bottom: 10px;
  font-size: 74px;
}
.glyphicon {
  position: relative;
  top: 1px;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: 400;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
.bs-glyphicons .glyphicon-class {
  display: block;
  text-align: center;
  word-wrap: break-word;
}
</style>
<div class="bs-docs-section">
  
  <p style='margin-left:17px'></p><br>
<div style='margin-left:20px' class="bs-glyphicons">
    <ul class="bs-glyphicons-list">
      

        <a href='#'><li>
          <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
          <span class="glyphicon-class">Dashboard</span>
        </li></a>


        <a href='surat-masuk.php'><li>
          <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
          <span class="glyphicon-class">Surat Masuk</span>
          <?php
				 include "koneksi.php";
				$sqlCommand = "SELECT COUNT(*) FROM surat_masuk"; 
				$query = mysqli_query($koneksi, $sqlCommand) or die (mysqli_error()); 
				$row = mysqli_fetch_row($query);
				echo "Data Surat Masuk : " . $row[0];
				mysqli_free_result($query); 
				mysqli_close($koneksi);
			?>
        </li></a>
    
        <a href='surat-keluar.php'><li>
          <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
          <span class="glyphicon-class">Surat Keluar</span>
            <?php
				 include "koneksi.php";
				$sqlCommand = "SELECT COUNT(*) FROM surat_keluar"; 
				$query = mysqli_query($koneksi, $sqlCommand) or die (mysqli_error()); 
				$row = mysqli_fetch_row($query);
				echo "Data Surat Keluar : " . $row[0];
				mysqli_free_result($query); 
				mysqli_close($koneksi);
			?>
        </li></a>

         <a href='#'><li>
          <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
          <span class="glyphicon-class">Report Surat Masuk</span>
        </li></a>
       
        <a href=''><li>
          <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
          <span class="glyphicon-class">User Aktif</span>
          <?php echo $_SESSION['username']; ?>
        </li></a>

    </ul>
  </div>
</div>


  </div>
</div>