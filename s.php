<title>Suratku</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> 
      <script src="js/jquery-3.2.1.js"></script>
    <script src="js/bootstrap.min.js"></script>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Isi Data Pelaporan</h4>
      </div>
      <div class="modal-body">
      <div class="panel-group">
      <div class="panel panel-success">
      <div class="panel-heading">Isi Data</div>
      <br>
    <form enctype="multipart/form-data"  class="form-horizontal" action="" method="post" >
    
        <!--
    <div class="form-group">
          <label class="col-sm-3 control-label">No Tiket</label>
          <div class="col-sm-2">
            <input type="text" name="id_keluhan" class="form-control" placeholder="No tiket" required>
          </div>
        </div>
        -->
    <div class="form-group">
          <label class="col-sm-3 control-label">No Tiket</label>
          <div class="col-sm-6">
            <input type="text" name="id_keluhan" value="<?php echo $hasilkode; ?>" class="form-control" readonly="readonly">
          </div>
        </div>
    <div class="form-group">
          <label class="col-sm-3 control-label">Nama</label>
          <div class="col-sm-6">
            <input type="text" name="nm_pengeluh" class="form-control" placeholder="Nama Pengeluh" required>
          </div>
        </div>
                <div class="form-group">
          <label class="col-sm-3 control-label">Tipe Keluhan</label>
          <div class="col-sm-3">
          <input type="radio" name="tipe" value="Permintaan">
          <label>Permintaan</label>
            <br>
            <input type="radio" name="tipe" value="Permasalahan">
            <label>Permasalahan</label>
            <br>
          </div>
          </div>
  
        <div class="form-group">
          <label class="col-sm-3 control-label">Cabang</label>
          <div class="col-sm-6">
            <select name="cabang" class="form-control" required>
             <option value="">-Pilih Cabang-</option>
                    <option value="Kantor Pusat">000 - Kantor Pusat</option>
                    <option value="Cabang Palu">001 - Cabang Palu</option>
                    <option value="Cabang ToliToli">002 - Cabang ToliToli</option>
                    <option value="Cabang Poso">003 - Cabang Poso</option>
                    <option value="Cabang Luwuk">004 - Cabang Luwuk</option>
                    <option value="Cabang Bungku">005 - Cabang Bungku</option>
                    <option value="Cabang Salakan">006 - Cabang Salakan</option>
                    <option value="Cabang Sigi">007 - Cabang Sigi</option>
                    <option value="Cabang Donggala">101 - Cabang Donggala</option>
                    <option value="Cabang Parigi">102 - Cabang Parigi</option>
                    <option value="Kantor Kas Lambunu">103 - Kantor Kas Lambunu</option>
                    <option value="Kantor Kas Labean">104 - Kantor Kas Labean</option>
                    <option value="Kantor Kas Tolai">105 -  Kantor Kas Tolai</option>
                    <option value="Cabang Buol ">201 - Cabang Buol</option>
                    <option value="Kantor Kas Soni">202 - Kantor Kas Soni</option>
                    <option value="Capem Paleleh">211 -  Capem Paleleh</option>
                    <option value="Capem Ampana">301 - Capem Ampana</option>
                    <option value="Kantor Kas Wakai">302 - Kantor Kas Wakai</option>
                    <option value="Kantor Kas Tentena">303 - Kantor Kas Tentena</option>
                    <option value="Capem Morowali">401 - Capem Morowali</option>
                    <option value="Cabang Banggai Laut">402 - Cabang Banggai Laut</option>
                    <option value="Kantor Kas Beteleme">403 - Kantor Kas Beteleme</option>
                    <option value="Kantor Kas Batui">404 - Kantor Kas Batui</option>
                    <option value="Kantor Kas Toili">405 - Kantor Kas Toili</option>
                    <option value="Kantor Kas Bahomotefe">501 -  Kantor Kas Bahomotefe</option>
                    
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label">Jenis Keluhan</label>
          <div class="col-sm-6">
            <select name="jenis_keluhan" value="" class="form-control"  >
            <option value="Jenis Keluhan">-Jenis Keluhan-</option>
                              <option value="User">User</option>
                              <option value="overide">Overide</option>
                              <option value="mutasi user">Mutasi User</option>
                              <option value="Jaringan">Jaringan</option>
                              <option value="pc">PC</option>
                              <option value="email">Email</option>
                              <option value="Blokir Kartu ATM">Blokir Kartu ATM</option>
                              <option value=" ATM Tertelan">ATM Tertelan</option>
                              <option value="ATM Offline">ATM Offline</option>
            </select>
          </div>
        </div>

        
        

<!--
        <div class="form-group">
          <label class="col-sm-3 control-label">Tanggal Keluhan</label>
          <div class="col-sm-2">
          <input type="text" id="tggl_keluhan" name="tggl_keluhan" readonly="readonly" value="<?php echo date('d/m/Y',time()); ?>"  style="background-color: #c5c5c5;" readonly>
          </div>
        </div>-->
        <div class="form-group">
          <label class="col-sm-3 control-label">Keterangan </label>
          <div class="col-sm-6">
            <textarea class="form-control" name="ket_keluhan" class="form-control" placeholder="Keterangan"></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label">Penerima Keluhan</label>
          <div class="col-sm-6">
            <input type="text" name="penerima_keluhan" class="form-control" placeholder="Penerima Keluhan">
          </div>
        </div>
               <div class="form-group">
          <label class="col-sm-3 control-label">Eskalasi Ke</label>
          <div class="col-sm-4">
          <?php
          include "koneksi.php";
          $query = mysqli_query($koneksi,"SELECT*FROM type_keluhan");
          $query2 = mysqli_fetch_array($query);

          ?>
                        <select name="eskalasi" value="" class="form-control"  >
                               <option value="--">--</option>
                              <option value="kacab">Kepala Cabang</option>

            </select>
        </div>
        </div>

                        <div class="form-group">
          <label class="col-sm-3 control-label">Keterangan</label>
          <div class="col-sm-4">
           <textarea name="hasil1" class="form-control"></textarea>
          </div>
        </div>


            <div class="form-group">
          <label class="col-sm-3 control-label">Telephon</label>
          <div class="col-sm-4">
            <input type="text" name="telepon" value="" class="form-control" >
          </div>
        </div>
            <div class="form-group">
          <label class="col-sm-3 control-label">Email</label>
          <div class="col-sm-4">
            <input type="text" name="email" value="" class="form-control" >
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label"></label>
          <div class="col-sm-4">
File yang di upload : <input type="file" name="fupload"><br>
Deskripsi File : <br>
<textarea name="deskripsi" rows="8" cols="40"></textarea><br>
</div>
</div>

      </div>
      </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-success" name="kirim" value="Simpan">
      </div>
      </form>
    </div>
  </div>
</div>	